
def is_num_sequence(num_str, step):
    """ Проверяет, является ли число последовательностью из step чисел. """
    first_val = int(num_str[:step])
    seq = ''.join([str(s) for s in range(first_val, first_val + len(num_str))])
    return seq.find(num_str) >= 0


def calc_num_pos(str_num):
    """ Рассчет позиции числа в цифровой последовательнсоти. """
    pos = sum([i * 9 * 10 ** (i - 1) for i in range(1, len(str_num))])
    pos += len(str_num) * (int(str_num) - 10 ** (len(str_num) - 1))

    return pos


def predict_min_pos(num_str, step, is_starts_with_nine=False):
    """
    Пытается просчитать минимальную позицию числа в бесконечной
    цифровой последовательности.
    :param num_str: число в виде строки
    :param step: количество цифр из которых
    может состоять каждый элемент последовательности (212223 -> step = 2)
    :param is_starts_with_nine: флаг для особой проверки
    для чисел, которые начинаются на 9
    """

    if is_starts_with_nine:
        num = str(int(num_str) - 1)     # если начинается с 9, то вычитаем 1
    else:
        num = num_str

    min_pos = -1
    for i in range(0, step):
        seq = num[i:step] + num[0:i]    # циклический сдвиг вправо на i
        first_seq_el = seq[:]
        val = int(seq)

        # не рассматриваем числа начинающийся с 0 после сдвига
        if seq[0] == "0":
            continue

        # создание проверочной последовательности
        n = len(num_str) // step + 2
        seq += ''.join([str(val + j) for j in range(1, n)])
        index = seq.find(num_str)   # поиск в ней входного числа num_str

        if index >= 0:
            pos = calc_num_pos(first_seq_el) + index
            if min_pos < 0:
                min_pos = pos
            if pos < min_pos:
                min_pos = pos

    return min_pos


def predict_pos_for_num_starts_with_9(num_str):
    """
    Пытается предугадать позицию некторых чисел, начинающихся на 9,
    путем сдвига чисел вправо с шагом от 2 до длины числа.
    Например: 91701 -> 1701 -> 169170171 -> 16_91701_71
    """
    for step in range(2, len(num_str)):
        for offset in range(0, len(num_str) // 2):
            val = int(num_str[offset:offset + step]) - 1
            seq = str(val)
            for k in range(1, len(num_str) // step + 2):
                seq += str(val + k)
            ix = seq.find(num_str)
            if ix >= 0:
                return calc_num_pos(str(val)) + ix

    return -1


def find_num_pos_in_inf_seq(num):
    """
    Находит первое вхождение числа в бесконечную цифровую последовательность.
    """

    num_str = str(num)
    if is_num_sequence(num_str, 1):     # если число упорядоченная посл. цифр
        return int(num_str[0])

    pos = calc_num_pos(num_str) + 1     # исходная позиция числа

    if num_str[0] == "9":
        t_pos = predict_pos_for_num_starts_with_9(num_str)
        pos = t_pos if t_pos > 0 else pos

    for i in range(2, len(num_str) + 1):    # цикл по количеству цифр для шага

        if num_str[0] == "9":      # дополнительная проверка для чисел с 9
            index = predict_min_pos(num_str, i, is_starts_with_nine=True)
            if 0 < index < pos:
                pos = index

        index = predict_min_pos(num_str, i)     # поиск первого вхождения
        if 0 < index < pos:
            return index + 1

    return pos + 1


def main():
    results = []

    num = input()
    while num:
        results.append(find_num_pos_in_inf_seq(int(num)))
        num = input()

    for res in results:
        print(res)

if __name__ == '__main__':
    main()
