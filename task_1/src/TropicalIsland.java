import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

public class TropicalIsland {


    private static final class MatrixIndices {
        private int i;
        private int j;

        MatrixIndices(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MatrixIndices that = (MatrixIndices) o;

            if (i != that.i) return false;
            return j == that.j;

        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + j;
            return result;
        }
    }


    /**
     * Класс на основе TreeMap для хранения элементов матрицы в отсортированном порядке, где
     * key в TreeMap - значение элемента матрицы, а value - список индексов матрицы,
     * соответсвтующих значению key.
     */
    private static final class MatrixElementsTreeMap {
        private TreeMap<Integer, List<MatrixIndices>> elements = new TreeMap<>();

        /* Помещает MatrixIndices(i, j) в начало списка, соответствующего ключу value. */
        void put(int value, int i, int j) {
            if (!elements.containsKey(value)) {
                elements.put(value, new LinkedList<MatrixIndices>());
            }
            LinkedList<MatrixIndices> list = (LinkedList<MatrixIndices>) elements.get(value);
            list.addFirst(new MatrixIndices(i, j));
        }

        /* Возвращает первый MatrixIndices из списка элементов, соответсвующего ключу value. */
        MatrixIndices get(int value) {
            LinkedList<MatrixIndices> list = (LinkedList<MatrixIndices>) elements.get(value);
            MatrixIndices res = list.removeFirst();
            if (list.isEmpty()) {
                elements.remove(value);
            }
            return res;
        }

        /* Возвращает первый ключ - наименьшее значение элементов матрицы. */
        int firstKey() {
            return elements.firstKey();
        }
    }

    /* Заполняет список активных элементов (activeElements) и матрицу с метками
    уровня воды (waterMarks) начальными данными. */
    private static void populateStartData(MatrixElementsTreeMap activeElements,
                                          int[][] waterMarks,
                                          int[][] landscape)
    {
        int n = waterMarks.length, m = waterMarks[0].length;

        // Заполняет элементами из первой и последней строки матрицы
        for (int j = 1; j < m - 1; j++) {
            waterMarks[0][j] = landscape[0][j];
            activeElements.put(landscape[0][j], 0, j);
            waterMarks[n - 1][j] = landscape[n - 1][j];
            activeElements.put(landscape[n - 1][j], n - 1, j);
        }

        // Заполняет элементами из первого и последнего столбца матрицы
        for (int j = 1; j < n - 1; j++) {
            waterMarks[j][0] = landscape[j][0];
            activeElements.put(landscape[j][0], j, 0);
            waterMarks[j][m - 1] = landscape[j][m - 1];
            activeElements.put(landscape[j][m - 1], j, m - 1);
        }

        // Остальные элементы заполняются метками MIN_VALUE
        for (int i = 1; i < n - 1; i++) {
            for (int j = 1; j < m - 1; j++) {
                waterMarks[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    /** Считает значение общего объема воды.
     * @param landscape карта высот поверхности отсрова
     * @return объем воды
     */
    public static int calculateVolume(final int[][] landscape) {
        int n = landscape.length, m = landscape[0].length;
        if (n * m <= 4) return 0;

        MatrixElementsTreeMap activeElements = new MatrixElementsTreeMap();
        int[][] waterMarks = new int[n][m];

        populateStartData(activeElements, waterMarks, landscape);

        int unvisitedCellsCount = (n - 2) * (m - 2);
        int volume = 0;

        while (unvisitedCellsCount > 0) {
            int curVal = activeElements.firstKey();         // Получаем минимальный элемент
            MatrixIndices mi = activeElements.get(curVal);

            // Цикл по всвем направлениям
            for (int i = 0; i < 4; i++) {
                int ix = mi.i, iy = mi.j;

                if (i == 0) ix++;
                else if (i == 1) ix--;
                else if (i == 2) iy--;
                else iy++;

                if (ix < 0 || iy < 0 || ix >= n || iy >= m) continue;

                // Если ячейка еще не исследована, то считаем ее объем и добавляем в спсиок активных
                if (waterMarks[ix][iy] == Integer.MIN_VALUE) {
                    waterMarks[ix][iy] = Math.max(landscape[ix][iy], curVal);
                    activeElements.put(waterMarks[ix][iy], ix, iy);
                    volume += waterMarks[ix][iy] - landscape[ix][iy];
                    unvisitedCellsCount--;
                }
            }
        }

        return volume;
    }


    static int[][] readMatrix(int n, int m) {
        int[][] res = new int[n][m];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                res[i][j] = sc.nextInt();
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int islandsNum = sc.nextInt();

        int[] results = new int[islandsNum];
        for (int i = 0; i < islandsNum; i++) {
            int n = sc.nextInt(), m = sc.nextInt();
            int[][] island = readMatrix(n, m);
            results[i] = calculateVolume(island);
        }

        for (int i = 0; i < islandsNum; i++) {
            System.out.println(results[i]);
        }
    }
}
